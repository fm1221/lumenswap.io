import logo from './images/centus.png';

export default {
  code: 'CENTUS',
  logo,
  web: 'centus.one',
  issuer: 'GD43GJWAV4WVMW5O4LL27FFFXF5SFNQY7NYPHOMHFHFNYAI2W6OTYEMU',
};
