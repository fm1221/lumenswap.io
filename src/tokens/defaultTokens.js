import XLM from './XLM';
import CNY from './CNY';
import USDC from './USDC';
import ARS from './ARS';
import BRL from './BRL';
import NGNT from './NGNT';
import CENTUS from './CENTUS';
import USD from './USD';
import EURT from './EURT';
import SLT from './SLT';
import RMT from './RMT';
import TERN from './TERN';
import USDT from './USDT';
import BTC from './BTC';
import ETH from './ETH';

export default [
  XLM,
  USDC,
  CNY,
  ARS,
  BRL,
  NGNT,
  CENTUS,
  USD,
  EURT,
  SLT,
  RMT,
  TERN,
  USDT,
  BTC,
  ETH,
];
